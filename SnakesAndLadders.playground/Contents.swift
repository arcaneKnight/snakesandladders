//: Playground - noun: a place where people can play

import UIKit

import Foundation

// Initial variables (just to get you started)
let destination: Int = 100
var numberOfPlayers = 5

var thewinner: Int = 0

// create a board with a '0' place as starting for the players
var board = [Int](count:destination+1, repeatedValue: 0)

var playerQ = [Int](count: numberOfPlayers, repeatedValue: 0)


// Functions
func boardSetup() {
    // set up ladders and snakes, based on an existing board
    // modifier values can be positive (ladder) or negetive (snakes)
    board[1] = +37; board[4] = 10
    board[9] = +22; board[28] = 56
    board[21] = 21; board[51] = 16
    board[71] = 20; board[80] = 20
    
    board[16] = -10; board[47] = -21
    board[49] = -38; board[56] = -3
    board[62] = -43; board[64] = -4
    board[87] = 24-87; board[93] = 73-93
    board[95] = 75-95; board[98] = 78-98
}

func rollDice() -> Int {
    // Generate a random number between 1 to 6
    // Hint: use the rand() function to get a random number, then limit it to between 1 to 6
    
    return Int(random()%6+1)
}


// Pass it a player number, rolled dice, etc
// and print out a description of the move
func printStep(playerNumber: Int, numRolled: Int, modifier: Int, originalPos: Int) {
    var modifierString = "finds a ladder and climbs"
    if modifier < 0 {
        modifierString = "is swallowed by a snake and slides"
    }
    print("Player \(playerNumber) rolls a \(numRolled) and moves to position #\(originalPos + numRolled).")
    if modifier != 0 {
        print("-- Player \(playerNumber) \(modifierString) \(modifier) step(s) to position #\(playerQ[playerNumber])")
    }
}

boardSetup()

print("board layout: \(board)")

var hasWinner: Bool = false

while (!hasWinner) {
    // For each player, find the current position, roll the dice,
    // set the new position based on the board's snakes and ladders
    // print out the move
    // rollDice()
    var numRolled : Int
    var numModifier : Int = 0
    var originalPos : Int = 0
    
    
    for playerNumber in 0..<playerQ.count {
        
        print("Player \(playerNumber)'s turn...")
        numRolled = rollDice()  // roll the die!
        print(" rolled: \(numRolled)")
        originalPos = playerQ[playerNumber]
        print(" current position: \(originalPos)")
        
        switch originalPos + numRolled {
            
        case destination:
            hasWinner = true
            numModifier = 0
            thewinner = playerNumber
            break;
            
        case let result where result > destination:
            // roll beyond the final square - out of bound
            
            // the players loses the turn and stays put
            break;
            
        default:
            playerQ[playerNumber] += numRolled
            numModifier = board[playerQ[playerNumber]]
            playerQ[playerNumber] += board[playerQ[playerNumber]]
            if playerQ[playerNumber] >= destination {
                hasWinner = true
                thewinner = playerNumber
            }
            
        }
        
        printStep(playerNumber,numRolled: numRolled,modifier: numModifier, originalPos: originalPos)
        
        if hasWinner {
            print("We have a winner! --> ",terminator: "")
            break
        }
    }
}

if hasWinner {
    print("Player number \(thewinner) wins!")
}


// re-roll:
/*            var numTry = 1
var iResult = 0
repeat {
numTry += 1
numRolled = rollDice()
print("  roll again...got \(numRolled)")
iResult = playerQ[playerNumber] + numRolled
} while (iResult > destination) && (numTry < 100)

if iResult == destination {
hasWinner = true
thewinner = playerNumber
}

if numTry >= 100 {
print("max number of retires reached")
numRolled = 0; numModifier = 0
}
*/


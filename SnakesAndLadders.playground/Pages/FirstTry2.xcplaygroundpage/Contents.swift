//: [Previous](@previous)

import Foundation

// var str = "Hello, playground"

struct square {
    var index = 0
    var modifier = 0
}

class board {
    let startSquare = 0
    var finalSquare = 100
    
    var squares = [square]()
    var playerQ = [Int]()
    //    var winnerFound : Bool = false
    
    func movePlayer(playerID: Int, numRolled: Int) -> (finalPos: Int,modifier: Int) {
        let currentPos = playerQ[playerID]
        var modifier = 0
        if currentPos + numRolled < finalSquare {
            modifier = squares[currentPos + numRolled].modifier
        }
        let finalPos = currentPos + numRolled + modifier;
        
        //        assert(finalPos <= finalSquare)
        //        if finalPos == finalSquare {
        //            winnerFound = true
        //        }
        
        return (finalPos, modifier)
    }
    
    func placePlayer(playerPosition: Int) -> Int {
        var playerID : Int
        
        playerQ.append(playerPosition)
        playerID = playerQ.endIndex.predecessor()
        
        return playerID
    }
}

struct diceRange {
    // length is inclusive of start
    var start = 1
    var length = 6
}

class dice {
    var cubeDie = diceRange()
    
    func roll() -> Int {
        return (Int(rand())%(cubeDie.length - cubeDie.start + 1)) + cubeDie.start
    }
}

var myBoard = board()
var myDie = dice()
var numRolled = 0

// set up the board
// set up ladders and snakes, based on an existing board

myBoard.finalSquare = 100

for index in 0...myBoard.finalSquare {
    
    myBoard.squares.append(square(index : index, modifier: 0))
    
    myBoard.squares[index].index = index
    
    switch index {
    case 4:
        myBoard.squares[index].modifier = 10
    case 9:
        myBoard.squares[index].modifier = 22
    case 21:
        myBoard.squares[index].modifier = 21
    case 28:
        myBoard.squares[index].modifier = 56
    case 51:
        myBoard.squares[index].modifier = 16
    case 71:
        myBoard.squares[index].modifier = 20
    case 80:
        myBoard.squares[index].modifier = 20
        
    case 16:
        myBoard.squares[index].modifier = -10
    case 47:
        myBoard.squares[index].modifier = -21
    case 49:
        myBoard.squares[index].modifier = -38
    case 56:
        myBoard.squares[index].modifier = -3
    case 62:
        myBoard.squares[index].modifier = -43
    case 64:
        myBoard.squares[index].modifier = -4
    case 87:
        myBoard.squares[index].modifier = 24-87
    case 93:
        myBoard.squares[index].modifier = 73-93
    case 95:
        myBoard.squares[index].modifier = 75-95
    case 98:
        myBoard.squares[index].modifier = 78-98
    default:
        myBoard.squares[index].modifier = 0
    }
}


// set up a die with 6 faces of 1 to 6
myDie.cubeDie.start = 1
myDie.cubeDie.length = 6

// put 3 players onto the board
var player1_ID = myBoard.placePlayer(0)
var player2_ID = myBoard.placePlayer(0)
var player3_ID = myBoard.placePlayer(0)


var moveResult = (finalPos: 0,modifier: 0)

// start the game!
// player1 goes first
var winnerFound = false
var theWinner : Int
var numTurns = 0
var modifierString : String

while (!winnerFound) {
    
    numTurns++
    
    print("===TURN #\(numTurns)")
    
    for playerNo in 0..<myBoard.playerQ.count {
        
        numRolled = myDie.roll()
        moveResult = myBoard.movePlayer(playerNo, numRolled: numRolled)
        
        print("  Player \(playerNo) is at position \(myBoard.playerQ[playerNo]) and rolls \(numRolled)")
        
        switch moveResult.modifier {
        case let result where result < 0:
            modifierString = "is devoured by a snake and slides \(moveResult.modifier) step(s)"
            break
        case let result where result > 0:
            modifierString = "finds a ladder and quickly climbs \(moveResult.modifier) step(s)"
            break
        default:
            modifierString = "finds nothing of interest"
            break
        }
        
        print("    Player \(playerNo) is now at \(myBoard.playerQ[playerNo] + numRolled) and \(modifierString)")
        print("    Player \(playerNo) moves to \(moveResult.finalPos)")
        
        switch moveResult.finalPos {
            
        case myBoard.finalSquare:
            winnerFound = true
            
        case let result where result > myBoard.finalSquare:
            // result overshot so the player stays at the current position
            // no update to the player position
            print("  Player \(playerNo) goes out of bound!  Player \(playerNo) returns to the current position")
            break;
            
        default:
            myBoard.playerQ[playerNo] = moveResult.finalPos
            break
            
        }
        if winnerFound {
            theWinner = playerNo
            print("Congratulations! Player \(theWinner) wins!")
            break
        }
    }
}


//: [Next](@next)
